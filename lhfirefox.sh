#!/bin/bash


# Uncomment these two variable assignments and supply info to automagically
# get the info into the copyleft.
PROGRAM_COPYLEFT="copyright (c) 2013-2022 der.hans";
PROGRAM_CONTACT_INFO="der.hans - https://floss.social/@FLOX_advocate"

TEMPLATE_COPYLEFT="copyright (c) 2000-2021 der.hans";
TEMPLATE_CONTACT_INFO="der.hans - https://floss.social/@FLOX_advocate"

show_copyleft() {

cat <<EOF
# lhfirefox.sh is a simple wrapper to start a specific Firefox profile.
# 
# lhfirefox.sh is part of lh-firefox-profile-manager.
#
# lhtemplate-shell.sh is s shell script template with copyright and several
* command line options in place. It currently requires bash to work.
#
# ${PROGRAM_COPYLEFT:+program ${PROGRAM_COPYLEFT}}
# ${TEMPLATE_COPYLEFT:+template ${TEMPLATE_COPYLEFT}}
#
# This script is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# See http://www.gnu.org/copyleft/gpl.html for the full text of the license.
#
# This script is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# ${PROGRAM_CONTACT_INFO:+program contact: ${PROGRAM_CONTACT_INFO}}
# ${TEMPLATE_CONTACT_INFO:+template contact: ${TEMPLATE_CONTACT_INFO}}

EOF

	# Set $DISCLAIMER if you have something to add.
	# DISCLAIMER=""
	if [ "$DISCLAIMER" ] ; then
		echo "#"
		echo "# $DISCLAIMER"
	fi

	exit
}

### Documentation ###
##
#

# Simply start adding code to the bottom of the script.

# Initialization can be changed in the functions below.
# initialize() is where default values should be initialized.
# get_conf() sources the config file.
# usage() echos basic syntax back to the calling shell.
# show_help() echos more verbose syntax back to the calling shell.
# parse_cl() parses command line parameters.
# See documentation in the functions for more verbose explanation.

# If you need an inline changelog, move this section up to the 3rd line of
# of the script.
###   START INLINE CHANGELOG   ###
##
# Script Creation Date: Unspecified
# Script Author: Unspecified
#
# Template Author: der.hans
#
# Copy the next three lines to track changes inline. Template Author feels
# it is better to use revision management such as subversion or RCS. See
# the note on Revision below for more info on tracking the file revision.
# Change Date:
# Change Author:
# Change Description:
##
###   END INLINE CHANGELOG   ###


### Functions ###
##
#

initialize() {
	# This is called before checking anything else.
	
	# Default values for variables not in the template can be set in
	# lx_initialize or here. To update default values for variables used
	# by the template, update them here. In any case, default values can
	# be overridden in the configuration file or from the command line.

	lx_initialize

	# Note: see the V option in parse_cl() for more info.
	# Note the single quotes to make sure the dollar signs
	# aren't interpreted.
	# Change Version to none or something else for a new project.
	Version='$Revision: 1.14 $'
	# Version='none'

	# $DEBUG and $VERBOSE can be used in your program to give more
	# output. They can be called additively, that is '-D -D -D' will
	# result in $DEBUG being set to 3.
	DEBUG=0
	VERBOSE=0
	# This pulls the basic shell script name from the full path
	# for the script which is stored in $0. It will get assigned
	# 'lhtemplate-shell.sh.sh' if the scriptname isn't changed.
	cmd=${0##*/}
	# cmd=`basename $0`
	# This strips the '.sh' off of $cmd, to give just the base word
	# of the name of this script. It will get assigned 'lhtemplate-shell' if
	# the scriptname isn't changed.
	basecmd=${cmd%.sh}
	# $cmdpath holds the path to where the script is in the filesystem.
	# If called via full-path, the $cmdpath will have the full-path from
	# '/'. If called via relative-path, then it will have the relative-path
	# from the directory in which the command was run.
	cmdpath=${0%/*}
	# cmdpath=`dirname $0`
	# Default configuration file is up a directory from the script and
	# then into the local 'etc/' directory. This matches the customization
	# of putting binaries in 'bin/' and configuration info in 'etc/'.
	# See get_conf() below for more info about configuration files.
	conffile=${cmdpath}/../etc/${basecmd}/${basecmd}.conf
	# Uncomment the next command if you want to default to /etc/ for the
	# default configuration file.
	# conffile=/etc/${basecmd}/${basecmd}.conf
}

get_conf() {
	# The configuration file can override the default values set in
	# initialize(), but still be overridden by the command line
	# parameters.
	
	# By default get_conf() looks up a directory from the script for
	# 'etc/' and looks for a configuration file named ${basecmd}.conf.
	# If the 

	# This might require GNU grep, so it's left disabled by default.
	# If you want to allow specifying the configuration file to use
	# from the command line, then enable the next few lines of code.
	# FIXME: '--conf <file>' doesn't get cleaned up from the command
	# line arguments, so it might cause problems.
	# echo $@ | grep -- ' --conf ' >/dev/null 2>&1
	# if [ 0 -eq $? ] ; then
		# RegEx: look for --conf with any number of spaces after it,
		# grab one or more non-spaces, toss the non-spaces into
		# $conffile, ignore everything on either side of the config
		# specification.
		# conffile=`echo $@ | sed -e 's/.*\(--conf  *\)\([^ ][^ ]*\).*/\2/'`
		# explicit_conffile='yes'
	# fi


	# If $conffile exists source it. See the 'source' function in the bash
	# documentation, e.g. 'man bash'.
	if [ -r "$conffile" ] ; then
		# NOTE: this is somewhat dangerous as commands in $conffile
		# will be executed. They're still executed as the person
		# running the command, so it really doesn't add any danger
		# that wasn't already there.
		source "$conffile"
	fi

	# By convention user level configuration files are kept in the $HOME
	# directory either in files starting with a period, followed by the
	# command name and ending with 'rc', or there is a directory starting
	# with a period, followed by the command name that holds the
	# user's configuration file.
	# 'hidden' files are those beginning with a period. Many commands
	# ignore them by default.
	# FIXME: find docs on hidden files to point to.

	# If the conf file was specifically called from the command line,
	# then don't read the user configuration file.

	local rcfile="$HOME/.${basecmd}/${basecmd}rc"
	if [ -r "$rcfile" -a 'yes' != "$explicit_conffile" ] ; then
		# See above for a warning about sourcing files.
		source "$rcfile" 
	fi
}

usage() {
	# usage() gets called if there are illegal command line parameters.
	# It gives a cursory description of valid command line options.
	# It should be updated when new command line options are added.
	# In general, one attempts to document the various combinations
	# available per command instance. Options that provide a function
	# and exit get their own line.
	# FIXME: find or create syntax documentation.
	cmd=${0##*/}
	# cmd=`basename $0`
	echo "usage: $cmd -h"
	echo "       $cmd -V"
	echo "       $cmd -C"
	echo "       $cmd [-D] [-v] [-P <profile>] [-o <URL>]" # [--conf <file>]"

	exit
}

show_help() {
	# show_help() is more verbose than usage() in that it describes
	# what the various options mean.
	echo "	-h		This help text."
	echo "	-D		Increase DEBUG one level."
	echo "	-v		Increase verbosity one level."
	echo "	-V		Print version."
	echo "	-C		Show copyleft."
	echo "	-P <profile>	Which profile to use."
	echo "	-o <URL>	Page to open in new tab."
	# echo "	--conf <file>	Use <file> as configuration file."

	exit
}

parse_cl() {
	# parse_cl() parses the command line for one letter options. It
	# doesn't handle multi-letter or double-dash options.
	# See the 'getopts' function in the bash manpage for more info
	# on how 'getopts' works.
	# Set valid command line options here.
	while getopts "hDvVCP:o:" c
	do
		case "${c}" in
			# Handle the command line options here. Don't forget to
			# update show_help() and usage().
			h) show_help	;;
			D) (( DEBUG++ ))
				# If using true bourne shell, /bin/sh, or
				# POSIX shell use the external calls to expr
				# rather than the double parens arithmetic
				# syntax that works in bash and ksh and is
				# internal to the shell.
				# D) DEBUG=$( expr $DEBUG + 1 )
				;;
			v) (( VERBOSE++ ))
				# If using true bourne shell, /bin/sh, or
				# POSIX shell use the external calls to expr
				# rather than the double parens arithmetic
				# syntax that works in bash and ksh and is
				# internal to the shell.
				# D) DEBUG=$( expr $DEBUG + 1 )
				# v) VERBOSE=`expr $VERBOSE + 1`
				echo "VERBOSE = $VERBOSE"
				;;
			V) echo $Version | sed -e 's/\$\bRevision: //' -e 's/ \$//'
				# That sed command passes 'none' through
				# untouched and munges RCS style revision
				# strings to output just the numbers.
				# Called here in order to avoid the extra
				# shell and two regexen unless they're	
				# actually used.
				#
				# The below doesn't work right if multiple
				# options were given with a single dash,
				# e.g. -DVD, but since usage() doesn't show
				# that as valid we'll ignore it.
				if [ $# -eq 1 ] ; then
					exit
				fi	;;
			C) show_copyleft	;;
			P) __profile="$OPTARG"
				__profile_args="-P ${__profile}"
				;;
			o) __url="$OPTARG"
				;;
			*) usage	;;
		esac
	done

}

### Program configuration can go here.
##
#

lx_initialize() {
	# Set throwaway variable to a random number
	example_variable=$RANDOM
}

###   MAIN   ###
##
#

# Setup
initialize
get_conf $@
parse_cl $@
shift $(( $OPTIND - 1 ))


### Start coding here ###
##
#

ulimit -m 8000000
ulimit -n 2048

if [ "$1" ] ;  then
        __profile="$1"
        __profile_args="-P ${__profile}"
# else
        # profile_args="-ProfileManager"
fi

if [ -n "${__url}" ] ; then
	firefox ${__profile_args} --new-tab "${__url}" >/dev/null 2>&1 &
else
	firefox -new-instance ${__profile_args} >/dev/null 2>&1 &
fi
