#!/bin/bash

TEMPLATE_COPYLEFT="copyright (c) 2000-2022 der.hans";
TEMPLATE_CONTACT_INFO="der.hans - https://floss.social/@FLOX_advocate"

# Uncomment these two variable assignments and supply info to automagically
# get the info into the copyleft.
PROGRAM_COPYLEFT="copyright (c) 2013-2022 der.hans";
PROGRAM_CONTACT_INFO="der.hans - https://floss.social/@FLOX_advocate"

show_copyleft() {

cat <<EOF
# lhffprofilemgr creates new Firefox profiles from example profiles.

# lhffprofilemgr uses the lhgetopts.sh shell script template.
# lhgetopts.sh is a shell script template with copyright and several
* command line options in place. It currently requires bash to work.
#
# ${TEMPLATE_COPYLEFT:+template ${TEMPLATE_COPYLEFT}}
# ${PROGRAM_COPYLEFT:+program ${PROGRAM_COPYLEFT}}
#
# lhffprofilemgr.sh is part lh-firefox-profile-manager.
#
# This script is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# See http://www.gnu.org/copyleft/gpl.html for the full text of the license.
#
# This script is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# ${TEMPLATE_CONTACT_INFO:+template contact: ${TEMPLATE_CONTACT_INFO}}
# ${PROGRAM_CONTACT_INFO:+program contact: ${PROGRAM_CONTACT_INFO}}

EOF

	# Set $DISCLAIMER if you have something to add.
	# DISCLAIMER=""
	if [ "$DISCLAIMER" ] ; then
		echo "#"
		echo "# $DISCLAIMER"
	fi

	exit
}

### Documentation ###
##
#

# Simply start adding code to the bottom of the script.

# Initialization can be changed in the functions below.
# initialize() is where default values should be initialized.
# get_conf() sources the config file.
# usage() echos basic syntax back to the calling shell.
# show_help() echos more verbose syntax back to the calling shell.
# parse_cl() parses command line parameters.
# See documentation in the functions for more verbose explanation.

# If you need an inline changelog, move this section up to the 3rd line of
# of the script.
###   START INLINE CHANGELOG   ###
##
# Script Creation Date: Unspecified
# Script Author: Unspecified
#
# Template Author: der.hans
#
# Copy the next three lines to track changes inline. Template Author feels
# it is better to use revision management such as subversion or RCS. See
# the note on Revision below for more info on tracking the file revision.
# Change Date:
# Change Author:
# Change Description:
##
###   END INLINE CHANGELOG   ###


### Functions ###
##
#

initialize() {
	# This is called before checking anything else.

	# Add initialization parameters to the lx_initialize function at
	# the bottom or add them to this function, lx_initialize called last
	# in this function.

	# Default values set during initialization can be overridden in the
	# configuration file or from the command line.

	# Note: see the V option in parse_cl() for more info.
	# Note the single quotes to make sure the dollar signs
	# aren't interpreted.
	# Change Version to none or something else for a new project.
	Version='$Revision: $'
	# Version='none'

	# $DEBUG and $VERBOSE can be used in your program to give more
	# output. They can be called additively, that is '-D -D -D' will
	# result in $DEBUG being set to 3.
	DEBUG=0
	VERBOSE=0
	# This pulls the basic shell script name from the full path
	# for the script which is stored in $0. It will get assigned
	# 'lhffprofilemgr.sh' if the scriptname isn't changed.
	cmd=${0##*/}
	# cmd=`basename $0`
	# This strips the '.sh' off of $cmd, to give just the base word
	# of the name of this script. It will get assigned 'lhffprofilemgr' if the
	# scriptname isn't changed.
	basecmd=${cmd%.sh}
	# $cmdpath holds the path to where the script is in the filesystem.
	# If called via full-path, the $cmdpath will have the full-path from
	# '/'. If called via relative-path, then it will have the relative-path
	# from the directory in which the command was run.
	cmdpath=${0%/*}
	# cmdpath=`dirname $0`
	# Default configuration file is up a directory from the script and
	# then into the local 'etc/' directory. This matches the customization
	# of putting binaries in 'bin/' and configuration info in 'etc/'.
	# See get_conf() below for more info about configuration files.
	conffile=${cmdpath}/../etc/${basecmd}/${basecmd}.conf

	# Uncomment the next command if you want to default to /etc/ for the
	# default configuration file.
	# conffile=/etc/${basecmd}/${basecmd}.conf

	lx_initialize
}

get_conf() {
	# The configuration file can override the default values set in
	# initialize(), but still be overridden by the command line
	# parameters.
	
	# By default get_conf() looks up a directory from the script for
	# 'etc/' and looks for a configuration file named ${basecmd}.conf.
	# If the 

	# This might require GNU grep, so it's left disabled by default.
	# If you want to allow specifying the configuration file to use
	# from the command line, then enable the next few lines of code.
	# FIXME: '--conf <file>' doesn't get cleaned up from the command
	# line arguments, so it might cause problems.
	# echo $@ | grep -- ' --conf ' >/dev/null 2>&1
	# if [ 0 -eq $? ] ; then
		# RegEx: look for --conf with any number of spaces after it,
		# grab one or more non-spaces, toss the non-spaces into
		# $conffile, ignore everything on either side of the config
		# specification.
		# conffile=`echo $@ | sed -e 's/.*\(--conf  *\)\([^ ][^ ]*\).*/\2/'`
		# explicit_conffile='yes'
	# fi


	# If $conffile exists source it. See the 'source' function in the bash
	# documentation, e.g. 'man bash'.
	if [ -r "$conffile" ] ; then
		# NOTE: this is somewhat dangerous as commands in $conffile
		# will be executed. They're still executed as the person
		# running the command, so it really doesn't add any danger
		# that wasn't already there.
		source "$conffile"
	fi

	# By convention user level configuration files are kept in the $HOME
	# directory either in files starting with a period, followed by the
	# command name and ending with 'rc', or there is a directory starting
	# with a period, followed by the command name that holds the
	# user's configuration file.
	# 'hidden' files are those beginning with a period. Many commands
	# ignore them by default.
	# FIXME: find docs on hidden files to point to.

	# If the conf file was specifically called from the command line,
	# then don't read the user configuration file.

	local rcfile="$HOME/.${basecmd}/${basecmd}rc"
	if [ -r "$rcfile" -a 'yes' != "$explicit_conffile" ] ; then
		# See above for a warning about sourcing files.
		source "$rcfile" 
	fi
}

usage() {
	# usage() gets called if there are illegal command line parameters.
	# It gives a cursory description of valid command line options.
	# It should be updated when new command line options are added.
	# In general, one attempts to document the various combinations
	# available per command instance. Options that provide a function
	# and exit get their own line.
	# FIXME: find or create syntax documentation.
	cmd=${0##*/}
	# cmd=`basename $0`
	echo "usage: $cmd -h"
	echo "       $cmd -V"
	echo "       $cmd -C"
	echo "       $cmd -b"
	echo "       $cmd -l"
	echo "       $cmd [-D] [-v] [-p <profile>] [-P <profile_args>] [-t <template_profile>]" # [--conf <file>]"
	# FIXME: add program options

	exit
}

show_help() {
	# show_help() is more verbose than usage() in that it describes
	# what the various options mean.
	echo "To setup $0, manually create a profile to be used as a template for"
	echo "  new profiles. The default is to use 'lhmuster'."
	echo "  'firefox --new-instance --ProfileManager' will open the profile manager."
	echo "To delete a profile remove the profile's directory under"
	echo "  $HOME/.mozilla/firefox then run '$cmd -b' to"
	echo "  rebuild the Firefox profile configuration file,"
	echo "  $HOME/.mozilla/firefox/profiles.ini"
	echo "	-h		This help text."
	echo "	-D		Increase DEBUG one level."
	echo "	-v		Increase verbosity one level."
	echo "	-V		Print version."
	echo "	-C		Show copyleft."
	echo "	-p <profile>	Name of profile."
	echo "	-P <profile_args>	Profile arguments override."
	echo "	-t <template_profile>	Profile to use as template."
	echo "	-b		Build profile ini file, use this after deleting a profile."
	echo "	-l		List current profiles."
	# echo "	--conf <file>	Use <file> as configuration file."

	exit
}

parse_cl() {
	# parse_cl() parses the command line for one letter options. It
	# doesn't handle multi-letter or double-dash options.
	# See the 'getopts' function in the bash manpage for more info
	# on how 'getopts' works.
	# Set valid command line options here.
	while getopts "hDvVCp:P:t:bl" c
	do
		case $c in   
			# Handle the command line options here. Don't forget to
			# update show_help() and usage().
			h) show_help	;;
			D) (( DEBUG++ ))
				# If using true bourne shell, /bin/sh, or
				# POSIX shell use the external calls to expr
				# rather than the double parens arithmetic
				# syntax that works in bash and ksh and is
				# internal to the shell.
				# D) DEBUG=$( expr $DEBUG + 1 )
				;;
			v) (( VERBOSE++ ))
				# If using true bourne shell, /bin/sh, or
				# POSIX shell use the external calls to expr
				# rather than the double parens arithmetic
				# syntax that works in bash and ksh and is
				# internal to the shell.
				# D) DEBUG=$( expr $DEBUG + 1 )
				# v) VERBOSE=`expr $VERBOSE + 1`
				echo "VERBOSE = $VERBOSE"
				;;
			V) echo $Version | sed -e 's/\$\bRevision: //' -e 's/ \$//'
				# That sed command passes 'none' through
				# untouched and munges RCS style revision
				# strings to output just the numbers.
				# Called here in order to avoid the extra
				# shell and two regexen unless they're	
				# actually used.
				#
				# The below doesn't work right if multiple
				# options were given with a single dash,
				# e.g. -DVD, but since usage() doesn't show
				# that as valid we'll ignore it.
				if [ $# -eq 1 ] ; then
					exit
				fi	;;
			C) show_copyleft	;;
			p) newProfile="$OPTARG"	;;
			P) profile_args="$OPTARG"	;;
			t) oldProfile="$OPTARG"	;;
			b) action="build"	;;
			l) action="listProfiles"	;;
			*) usage	;;
		esac
	done

}

makeRandomString() {
	unset randomString

	# Only allow letters, numbers, _ and - in the random string.
	# Try apg
	randomStringerizer=$( type -p apg 2>/dev/null )
	if [ -n "$randomStringerizer" ] ; then
		randomString=$( $randomStringerizer -n 1 -m $randomStringLength | tr -dc '[:alnum:]_-' | head -c $tokenLength )
	else
		# Try base64 of /dev/urandom
		randomStringerizer=$( type -p base64 2>/dev/null )
		if [ -n "$randomStringerizer" ] ; then
			randomString=$( dd if=/dev/urandom bs=1 count=$randomStringLength 2>/dev/null | base64 -w 0 | tr -dc '[:alnum:]_-' | head -c $tokenLength )
		else
			# Try rawish data from /dev/urandom
			randomString=$( dd if=/dev/random bs=1 count=$randomStringLength | tr -dc '[:alnum:]_-' | head -c $tokenLength )
		fi
	fi
}

checkProfile() {
	local __profile="${1}"
	# FIXME: change this to a stat
	profileExists=$( ls -d "$ffDir/"*".${__profile}" 2>/dev/null )
	if [ -n "${profileExists}" ] ; then
		return 0
	else
		return 1
	fi
}

addEntry() {
	# heredoc lines need to stay left-aligned
cat <<NEWPROFILE >>"${ffInifile}.new"

[Profile$iter]
Name=${iterProfile#*.}
IsRelative=1
Path=$iterProfile
NEWPROFILE
}

buildIni() {
	local iter=0
	local iterProfile=''
	cp $ffInifile ${ffInifile}.bak

	# heredoc lines need to stay left-aligned
cat <<NEWINI >>"${ffInifile}.new"
[General]
StartWithLastProfile=1
NEWINI

	if [ ${VERBOSE:-0} -gt 0 -o ${DEBUG:-0} -gt 0 ] ; then
		echo "Started in '$PWD'"
	fi
	cd $ffDir
	if [ ${VERBOSE:-0} -gt 0 -o ${DEBUG:-0} -gt 0 ] ; then
		echo "Now in Firefox profile directory, '$PWD'"
	fi
	for iterProfile in $( ls -d */ | grep -Ev '^example\.|^profiles\.' | sed -r 's@/$@@' | grep -F . | sort -t. -k2 ); do
		if [ ${VERBOSE:-0} -gt 1 -o ${DEBUG:-0} -gt 1 ] ; then
			echo $iterProfile
		fi
		addEntry
		let iter++
	done
	# move it
	mv $ffInifile $ffInifile.bak
	# move it, require approval
	mv -i $ffInifile.new $ffInifile
}

createProfile() {
	if [ -z "$newProfile" ] ; then
		echo "Please provide a profile name with the '-p' option." >&2
		usage
		exit 1
	fi

	checkProfile "${newProfile}"
	retstat=$?
	# we don't want an existing profile name, profile names need to be unique
	if [ 0 -eq $retstat ] ; then
		echo "ERROR: profile, $newProfile, alraedy exists, $profileExists." >&2
		exit 3
	fi
	
	makeRandomString
	if [ -z "$randomString" ] ; then
		echo "ERROR: failed to generate or capture a random string for the new profile." >&2
		exit 2
	fi

	if [ -d "$exampleProfileFQDir" -a ! -e "$ffDir/$randomString.$newProfile" ] ; then
		cp -pir "$exampleProfileFQDir" "$ffDir/$randomString.$newProfile"
	else
		if [ ! -d "$exampleProfileFQDir" ] ; then
			echo "'$exampleProfileFQDir' for '$exampleProfile' does not exist." >&2
			echo "See help ( $cmd -h ) for directions on creating a template profile." >&2
			exit 1
		elif [ -e "'$ffDir/$randomString.$newProfile'" ] ; then
		   	echo "'$ffDir/$randomString.$newProfile' somehow already exists even though '$newProfile' didn't show up, reality is exceeding expectations" >&2
		else
			echo "this shouldn't happen, it broke in an unexpected way" >&2
		fi
		usage
		exit 1
	fi

	# string replace of $exampleProfile with $newProfile
	find "${ffDir}/${randomString}.${newProfile}" -type f -exec sed -i -re "s/${exampleProfileDir}/${randomString}.${newProfile}/g" -e "s/${exampleProfile}/${newProfile}/g" {} +

	buildIni
}

listProfiles() {
	# Things that can be ignored
	## Firefix dirs and files: Crash Reports, Pending Pings, profiles.ini*,
	### installs.ini
	## lhfirefox dirs and files: lhParked, lhRemove
	# grep out files and directories to ignore that have a period in the name
	local ignoreDirs='\.ini$|profiles.ini'
	ls ${ffDir} | grep -Ev "${ignoreDirs}" | awk -F. '/\./ {print $2, $1}' | sort
}

lx_initialize() {
	profile_args='-P' # ProfileManager'
	randomStringLength=32
	tokenLength=12
}

###   MAIN   ###
##
#

# Setup
initialize
get_conf $@
parse_cl $@
shift $(( $OPTIND - 1 ))

### Start coding here ###
##
#

# Default to creating a new profile

mozDir="$HOME/.mozilla"
ffDir="$mozDir/firefox"
ffInifile="$ffDir/profiles.ini"
exampleProfile="${oldProfile:-lhmuster}"
exampleProfileFQDir=$( ls -d "$ffDir/"*".$exampleProfile" 2>/dev/null )
exampleProfileDir="${exampleProfileFQDir##*/}"

# FIXME: make sure requisite directories exist
# FIXME: add support to use containers

if [ ! -d $ffDir/ ] ; then
	echo "Expecting '$ffDir/' to be a directory with the Firefox configuration files." >&2
	exit 1
fi

# to delete a profile remove the directory and rebuid profiles.ini with -b
if [ 'build' = "$action" ] ; then
	buildIni
	exit
elif [ 'listProfiles' = "$action" ] ; then
	listProfiles
	exit
else
	createProfile
fi
